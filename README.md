# README #

> "The [`METACODES.PRO`](http://www.metacodes.pro) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers."

## What is this repository for? ##

***This repository contains `jekyll` working files being the source of the static `metacodes.bitbucket.org` blog for the [`METACODES.PRO`](http://www.metacodes.pro) OSS projects (or in case of the actual `metacodes.bitbucket.org` web-site the it contains the generated `jekyll` web-site files).***

## How do I get set up? ##

Install `jekill` as of the [installation](http://jekyllrb.com/docs/installation) instructions and as decribed by [so-simple-theme](https://github.com/mmistakes/so-simple-theme#ruby-gem-method):

```
gem install jekyll
gem install bundler
bundle install
```

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/metacodez/metacodes-meta/issues)
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`METACODES.PRO`](http://www.metacodes.pro) meta group of artifacts is published under some open source licenses; covered by the  [`metacodes-licensing`](https://bitbucket.org/metacodez/metacodes-licensing) ([`org.metacodes`](https://bitbucket.org/metacodez) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.

## Credits ##

This blog has been set up using [Michael Rose](https://github.com/mmistakes)'s great [So Simple Theme](http://mmistakes.github.io/so-simple-theme/theme-setup) theme; powered by the [jekyll](http://jekyllrb.com) templating engine for static web-sites ("*Jekyll • Simple, blog-aware, static sites*").

Below find the licensing terms and conditions of the *So Simple Theme*:

---

*The MIT License (MIT)*

*Copyright (c) 2014 Michael Rose*

*Permission is hereby granted, free of charge, to any person obtaining a copy*
*of this software and associated documentation files (the "Software"), to deal*
*in the Software without restriction, including without limitation the rights*
*to use, copy, modify, merge, publish, distribute, sublicense, and/or sell*
*copies of the Software, and to permit persons to whom the Software is*
*furnished to do so, subject to the following conditions:*

*The above copyright notice and this permission notice shall be included in all*
*copies or substantial portions of the Software.*

*THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*
*IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,*
*FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THEv
*AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*
*LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,*
*OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE*
*SOFTWARE.*
---
